package com.elkinduranm.MudanzaBackend.converter;

import java.util.List;
import java.util.ArrayList;
import org.springframework.stereotype.Component;
import com.elkinduranm.MudanzaBackend.entity.EjecucionEntity;
import com.elkinduranm.MudanzaBackend.model.EjecucionModel;

@Component("Convertidor")
public class Convertidor {

	public List<EjecucionModel> MappinLista(List<EjecucionEntity> ejecucionEntity){
		List<EjecucionModel> ejecucionmodel=new ArrayList<>();
		
		for(EjecucionEntity ejecucion:ejecucionEntity) {
			ejecucionmodel.add(new EjecucionModel(ejecucion));
		}
		return ejecucionmodel;
	}
	
	public EjecucionEntity ProfileEjecucion(EjecucionModel ejecucionModel) {
		EjecucionEntity ejecucion=new EjecucionEntity();
		ejecucion.setCedula(ejecucionModel.getCedula());
		ejecucion.setFechaEjecucion(ejecucionModel.getFechaEjecucion());
		return ejecucion;
	}
}
