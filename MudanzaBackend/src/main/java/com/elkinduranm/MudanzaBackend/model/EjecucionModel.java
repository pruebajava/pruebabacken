package com.elkinduranm.MudanzaBackend.model;


import java.time.LocalDateTime;

import com.elkinduranm.MudanzaBackend.entity.EjecucionEntity;


public class EjecucionModel {
	public EjecucionModel() {
		// TODO Auto-generated constructor stub
	}
	public EjecucionModel(EjecucionEntity ejecucion) {
		Id = ejecucion.getId();
		this.fechaEjecucion = ejecucion.getFechaEjecucion();
		this.cedula = ejecucion.getCedula();
	}
	public EjecucionModel(long id, LocalDateTime fechaEjecucion, long cedula) {
		super();
		Id = id;
		this.fechaEjecucion = fechaEjecucion;
		this.cedula = cedula;
	}
	private long Id;
	private LocalDateTime fechaEjecucion;
	private long cedula;
	public long getId() {
		return Id;
	}
	public void setId(long id) {
		Id = id;
	}
	public LocalDateTime getFechaEjecucion() {
		return fechaEjecucion;
	}
	public void setFechaEjecucion(LocalDateTime fechaEjecucion) {
		this.fechaEjecucion = fechaEjecucion;
	}
	public long getCedula() {
		return cedula;
	}
	public void setCedula(long cedula) {
		this.cedula = cedula;
	}
	
	
}
