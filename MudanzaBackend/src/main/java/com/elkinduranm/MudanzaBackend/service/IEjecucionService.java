package com.elkinduranm.MudanzaBackend.service;



import java.util.ArrayList;

import org.springframework.web.multipart.MultipartFile;

import com.elkinduranm.MudanzaBackend.model.EjecucionModel;

public interface IEjecucionService {
	Boolean RegitraEjecucion(EjecucionModel ejecucion);
	String prueba();
	String upload(MultipartFile file,long cedula);
	ArrayList<String> ReadFile(String nombrefile);
	String GeneraRespuesta(ArrayList<String> respuesta,EjecucionModel ejecucion);
	
}
