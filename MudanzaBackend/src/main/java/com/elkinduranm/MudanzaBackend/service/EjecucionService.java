package com.elkinduranm.MudanzaBackend.service;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Scanner;

import com.google.gson.Gson;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.elkinduranm.MudanzaBackend.converter.Convertidor;
import com.elkinduranm.MudanzaBackend.entity.EjecucionEntity;
import com.elkinduranm.MudanzaBackend.model.EjecucionModel;
import com.elkinduranm.MudanzaBackend.repository.EjecucionRepository;

@Service("Servicio")
public class EjecucionService implements IEjecucionService {
	@Autowired
	@Qualifier("Repositorio")
	private EjecucionRepository ejecutarRepository;
	
	@Autowired
	@Qualifier("Convertidor")
	private Convertidor convertidor;
	
	private String fechaString;
	private LocalDateTime fecha;
	 Gson gson;

	public EjecucionService() {
		// TODO Auto-generated constructor stub
		 fecha= LocalDateTime.now();
		 fechaString=fecha.toString();
		 fechaString=fechaString.replace("-", "");
		 fechaString=fechaString.replace(":", "");
		 fechaString=fechaString.replace(".", "");
		 gson=new Gson();
	}
	@Override
	public Boolean RegitraEjecucion(EjecucionModel ejecucion) {
		try {
			EjecucionEntity entity=convertidor.ProfileEjecucion(ejecucion);
			ejecutarRepository.save(entity);
			return true;
		} catch (Exception e) {
			// TODO: handle exception
			return false;
		}
	}

	@Override
	public String prueba() {
		// TODO Auto-generated method stub
		return "ok";
	}

	@Override
	public String upload(MultipartFile file, long cedula) {
		try {
			
			StringBuilder builder=new StringBuilder();
			builder.append(System.getProperty("user.home"));
			builder.append(File.separator);
			builder.append("upload");
			builder.append(File.separator);
			File directorio = new File(builder.toString());
			if (!directorio.exists()) {
	            if (directorio.mkdirs()) {
	                System.out.println("Directorio creado");
	            } else {
	                System.out.println("Error al crear directorio");
	            }
	        }
			builder.append(cedula+fechaString+file.getOriginalFilename());
			
			Path path=Paths.get(builder.toString());
	        
	        
			
			System.out.println(builder.toString());
			byte[] filebytes=file.getBytes();
			Files.write(path, filebytes);
			return builder.toString();
		} catch (Exception e) {
			return null;
			// TODO: handle exception
		}
	}
	@Override
	public ArrayList<String> ReadFile(String nombrefile) {
	try {
		File file = new File(nombrefile);
		
		Scanner scanner;
		ArrayList<String> respuesta=new ArrayList<>();
		scanner = new Scanner(file);
		
		ArrayList<ArrayList<Integer>> dt=new ArrayList<>();
		
		ArrayList<Integer> elementos=null;
		int cantidadElemtos=0;
		int fila=0;
		int objetos=0;
		while (scanner.hasNextLine()) {
			
			String linea = scanner.nextLine();
			if(fila<1) {
				System.out.println("Dìas laborales:"+linea);
			}else if(cantidadElemtos==0) {
				elementos = new ArrayList<>();
				cantidadElemtos=Integer.parseInt(linea);
				
			}else {
				elementos.add(Integer.parseInt(linea));
								
				objetos++;
				if(objetos==cantidadElemtos) {
					dt.add(elementos);
					cantidadElemtos=0;
					objetos=0;
				}
			}
			fila++;
		}		
		
		scanner.close();
		
		int i=1;
		for(ArrayList<Integer> dia:dt) {
			int resp= calcularViajes(dia,0);
			respuesta.add("Case #"+i+":"+resp);
			System.out.println("Case #"+i+":"+resp);
			i++;
		}
		String dtJson = gson.toJson(respuesta);
		System.out.println(dtJson);
		return respuesta;
	} catch (Exception e) {
		System.out.println("---"+e.toString());
		return null;
		// TODO: handle exception
	}
	}
	private int calcularViajes(ArrayList<Integer> dia,int viajes) {
		try {
			
			String[] mayinde=NumeroMayor(dia).split(",");
			int mayor=Integer.parseInt(mayinde[0]);
			dia.remove(Integer.parseInt(mayinde[1]) );
						
			int peso=0;
			int nElementos=1;
			
			while(peso<50 && mayor<50 ) {
				
				if(dia.size()==0)return 0;
				
				String[] mininde=NumeroMenor(dia).split(",");
				
				dia.remove(Integer.parseInt(mininde[1]) );
				nElementos++;
				peso=mayor*nElementos;
			}
			
			viajes++;
			
			if(dia.size()>1) viajes=calcularViajes(dia,viajes);
			return viajes;
			
			
		} catch (Exception e) {
			// TODO: handle exception
			System.out.println("+++"+e.toString());
			return 0;
		}
		
	}
	private String NumeroMenor(ArrayList<Integer> dia) {
		int menor=dia.get(0);
		int index=0;
		int i=0;
		for(Integer elemento:dia) {
			
			if(elemento<menor) {
				
				menor=elemento;
				index=i;
			}
			i++;
		}
		String respuesta= menor+","+index;
		return respuesta;
	}
	private String NumeroMayor(ArrayList<Integer> dia) {
		// TODO Auto-generated method stub
		
		int mayor=0;
		int index=0;
		int i=0;
		for(Integer elemento:dia) {
			if(mayor<elemento) {
				mayor=elemento;
				index=i;
			}
			i++;
		}
		String respuesta= mayor+","+index;
		return respuesta;
	}
	@Override
	public String GeneraRespuesta(ArrayList<String> respuesta, EjecucionModel ejecucion) {
		FileWriter flwriter = null;
		try {
			String nombre=ejecucion.getCedula()+fechaString+"lazy_loading_example_output.txt";
			
			StringBuilder builder=new StringBuilder();
			builder.append(System.getProperty("user.home"));
			builder.append(File.separator);
			builder.append("dowload");
			builder.append(File.separator);
			File directorio = new File(builder.toString());
			if (!directorio.exists()) {
	            if (directorio.mkdirs()) {
	                System.out.println("Directorio creado");
	            } else {
	                System.out.println("Error al crear directorio");
	            }
	        }
			builder.append(nombre);
			flwriter = new FileWriter(builder.toString());
			BufferedWriter bfwriter = new BufferedWriter(flwriter);
			for(String item:respuesta) {
				bfwriter.write(item);
				bfwriter.write("\n");
			}
			bfwriter.close();
			return nombre;
			
		} catch (Exception e) {
			// TODO: handle exception
			return null;
		} finally {
			if (flwriter != null) {
				try {
					flwriter.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
	}
	
	

	
}
