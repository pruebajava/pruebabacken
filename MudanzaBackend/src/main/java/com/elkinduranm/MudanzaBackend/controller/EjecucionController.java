package com.elkinduranm.MudanzaBackend.controller;


import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.LocalDateTime;
import java.util.ArrayList;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.elkinduranm.MudanzaBackend.model.EjecucionModel;
import com.elkinduranm.MudanzaBackend.model.RespuestaDto;
import com.elkinduranm.MudanzaBackend.service.IEjecucionService;

@RestController

@RequestMapping("/api")
public class EjecucionController {

	@Autowired
	private IEjecucionService service;
	
	@GetMapping("/prueba")
	public String prueba() {
		return service.prueba();
	}
	
	@CrossOrigin(origins = "*")
	@GetMapping("/dowload")
	public void dowloadFile(@RequestParam String fileName,HttpServletResponse res)  {
		System.out.println("archivo"+fileName);
			res.setHeader("Content-Disposition", "attachment; filename="+fileName);
			try {
				res.getOutputStream().write(contentOf(fileName));
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		
	}
	
	private byte[] contentOf(String fileName)  {
		StringBuilder builder=new StringBuilder();
		builder.append(System.getProperty("user.home"));
		builder.append(File.separator);
		builder.append("dowload");
		builder.append(File.separator);
		builder.append(fileName);
		
		try {
			Path path = Paths.get(builder.toString());
			return Files.readAllBytes(path);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			
			System.out.println("URISyntaxException contentOf:"+e.getMessage());
			return null;
		}
	}

	@CrossOrigin(origins = "*")
	@PostMapping("/ejecucion")
	public ResponseEntity<RespuestaDto> ejecucion(@RequestParam("file") MultipartFile file,RedirectAttributes atributos,
			@RequestParam("cedula") long cedula) {
		
			LocalDateTime fecha= LocalDateTime.now();
			
			RespuestaDto respuest=new RespuestaDto();
			String nombrefile=service.upload(file, cedula);
			String path="";
			if(nombrefile!=null) {
				
				EjecucionModel ejecucionModel=new EjecucionModel(); 
				ejecucionModel.setCedula(cedula);
				ejecucionModel.setFechaEjecucion(fecha);
				
				boolean guardaproceso=service.RegitraEjecucion(ejecucionModel);
				if(!guardaproceso)  System.out.println("no se registrò el proceso");
				//leer file
				ArrayList<String>respuesta=service.ReadFile(nombrefile);
				path=service.GeneraRespuesta(respuesta, ejecucionModel);
				respuest.setFileName(path);
				
			}
				return new ResponseEntity<>(
						respuest, 
					      HttpStatus.OK);
					
			
		
		
		
	}
}
