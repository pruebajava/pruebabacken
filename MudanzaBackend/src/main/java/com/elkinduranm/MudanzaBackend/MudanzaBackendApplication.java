package com.elkinduranm.MudanzaBackend;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MudanzaBackendApplication {

	public static void main(String[] args) {
		SpringApplication.run(MudanzaBackendApplication.class, args);
	}

}
