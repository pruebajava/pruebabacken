package com.elkinduranm.MudanzaBackend.repository;

import java.io.Serializable;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.elkinduranm.MudanzaBackend.entity.EjecucionEntity;

@Repository("Repositorio")
public interface EjecucionRepository extends JpaRepository<EjecucionEntity,Serializable>{
	public abstract EjecucionEntity findByCedula(long cedula);
	public abstract List<EjecucionEntity> findAll();
}
