package com.elkinduranm.MudanzaBackend.entity;

import java.io.Serializable;
import java.time.LocalDateTime;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Table;
import javax.persistence.Id;

@Table(name="EjecucionEntity")
@Entity
public class EjecucionEntity implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public EjecucionEntity() {
		// TODO Auto-generated constructor stub
	}
	public EjecucionEntity(long id, LocalDateTime fechaEjecucion, long cedula) {
		Id = id;
		this.fechaEjecucion = fechaEjecucion;
		this.cedula = cedula;
	}
	@GeneratedValue
	@Id
	private long Id;
	private LocalDateTime fechaEjecucion;
	private long cedula;
	
	

	

	public long getId() {
		return Id;
	}

	public void setId(long id) {
		Id = id;
	}

	public LocalDateTime getFechaEjecucion() {
		return fechaEjecucion;
	}

	public void setFechaEjecucion(LocalDateTime fechaEjecucion) {
		this.fechaEjecucion = fechaEjecucion;
	}

	public long getCedula() {
		return cedula;
	}

	public void setCedula(long cedula) {
		this.cedula = cedula;
	}
	
	
}
